package com.example.myapplication;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.RandomAccess;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AddYourGoal extends AppCompatActivity {

    private Button saveBtn;
    private EditText nameOfGoal;
    private RadioGroup groupOfButton;
    private  EditText contentsGoal;
    private RadioButton quantitiveGoal;
    private RadioButton finalGoaltext;
    private EditText descriptionOfGoal;
    private EditText numberOfDays;
    private EditText stepOfGoal;
    private EditText description;
    FinalGoal finalGoal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_your_goal);
        getSupportActionBar().hide();

        saveBtn = findViewById(R.id.save_goal_btn);
        nameOfGoal = findViewById(R.id.name_of_goal);
        groupOfButton = findViewById(R.id.group_of_btn);
        contentsGoal = findViewById(R.id.contents_goal_text);
        quantitiveGoal = findViewById(R.id.quantitive_goal);
        finalGoaltext = findViewById(R.id.final_goal);
        descriptionOfGoal = findViewById(R.id.description_of_goal_text);
        numberOfDays = findViewById(R.id.number_of_goal_text);
        stepOfGoal = findViewById(R.id.step_of_goal_text);
        description = findViewById(R.id.description_of_goal_text);
        finalGoal = new FinalGoal();

        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(numberOfDays.getText().toString() == null || nameOfGoal.getText().toString() == null || description.getText().toString() == null ||
                        numberOfDays.getText().length() == 0 || nameOfGoal.getText().length() == 0  || description.getText().length() == 0  ){
                    System.out.println("PUSTE WARTOŚCI&&");
                }
                finalGoal.setDays(Integer.parseInt(numberOfDays.getText().toString()));
                finalGoal.setName(nameOfGoal.getText().toString());
                finalGoal.setDescription(description.getText().toString());

                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl("http://51.68.136.95:8080")
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
                final Json json = retrofit.create(Json.class);
               // Call<FinalGoal> call = json.addGoal("19","sjhdgf","skdjh","sdfh");
            }
        });
    }
}
