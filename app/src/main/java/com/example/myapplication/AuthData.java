package com.example.myapplication;

public class AuthData {
    private String login;
    private String password;

    public AuthData(String login, String password) {
        this.setLogin(login);
        this.setPassword(password);
    }
    public AuthData(){
        System.out.println("BEZARGUMENTOWY LOLOL");
    }


    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
