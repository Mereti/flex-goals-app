package com.example.myapplication;

import java.util.Date;

public class FinalGoal {
    private Integer id;
    private Integer id_user;
    private Date date;
    private boolean is_shared;
    private String name;
    private String description;
    private String goal;
    private Integer days;
    private Integer points;
    private String progress;

    public FinalGoal(Integer id, Integer id_user, Date date, boolean is_shared, String name, String description, String goal, Integer days, Integer points, String progress) {
        this.setId(id);
        this.setId_user(id_user);
        this.setDate(date);
        this.setIs_shared(is_shared);
        this.setName(name);
        this.setDescription(description);
        this.setGoal(goal);
        this.setDays(days);
        this.setPoints(points);
        this.setProgress(progress);
    }
    public FinalGoal(){
        System.out.println("Bezargumentowy");
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId_user() {
        return id_user;
    }

    public void setId_user(Integer id_user) {
        this.id_user = id_user;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public boolean isIs_shared() {
        return is_shared;
    }

    public void setIs_shared(boolean is_shared) {
        this.is_shared = is_shared;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getGoal() {
        return goal;
    }

    public void setGoal(String goal) {
        this.goal = goal;
    }

    public Integer getDays() {
        return days;
    }

    public void setDays(Integer days) {
        this.days = days;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public String getProgress() {
        return progress;
    }

    public void setProgress(String progress) {
        this.progress = progress;
    }

}
