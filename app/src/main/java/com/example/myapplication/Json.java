package com.example.myapplication;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface Json {

    @POST("api/users/login-user")
    Call<UserInfoDate> getLogin(@Query("login") String login,@Query("password") String password);

    @Headers("Content-type: application/json")
    @POST("/api/users/login-user")
    Call<User> login(@Body AuthData json);

    //rejestracja
    @POST("api/users/register-user")
    Call<Post> postRegister(@Query("login") String login, @Query("password") String password);

    //pobieranie danych uzytkownika
    @GET("api/users/priv-data/{id}")
    Call<Post> getUserData(@Query("id") Integer id);


    @PUT("api/users/priv-data/update-name")
    Call<Post> putName(@Query("name") String name);

    @GET("api/goals/predefined-quantitative")
    Call <List<PredefinedQuantitativeGoal>> getPredefiniedFinal();

    @PUT("api/users/priv-data/update-name")
    Call<UserData> updateUserName(@Body UserData d);

    @PUT("api/users/priv-data/update-sex")
    Call<UserData> updateUserSex(@Body UserData d);
//ILOSCIOWE

    @GET("api/goals/quantitative/{id}")
    Call<List<QuantitativeGoalFlag>> getQuantitiveGoalsFlag(@Path("id") Integer id);



//ZALICZENIOWE

    @GET("api/goals/final/{id}")
    Call<List<FinalGoalFlag>> getGoalsFlag(@Path("id") Integer id);

    @DELETE("api/goals/final-delete/{id}")
    Call<ResponseBody> deleteGoal(@Path("id") Integer id);

    @POST("api/goals/final-add")
    Call<FinalGoal> addGoal(@Body FinalGoal json);
    }




