package com.example.myapplication;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginActivity extends AppCompatActivity {
    public static final String EXTRA_ID ="com.example.myapplication.EXTRA_ID";
    public static final String EXTRA_POINTS ="com.example.myapplication.EXTRA_POINTS";
    public static final String EXTRA_LOGIN ="com.example.myapplication.EXTRA_LOGIN";

    private EditText loginText;
    private EditText passwordText;
    private Button login;
    private Button register;
    private TextView loginOrRegister;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);

        loginText = findViewById(R.id.loginIn);
        passwordText = findViewById(R.id.passwordIn);
        login = findViewById(R.id.logBtn);
        register = findViewById(R.id.registerBtn);
        loginOrRegister=findViewById(R.id.viewPager);

        ArrayList<View> viewsToFadeIn = new ArrayList<View>();

        viewsToFadeIn.add(findViewById(R.id.logBtn));
        viewsToFadeIn.add(findViewById(R.id.registerBtn));
        viewsToFadeIn.add(findViewById(R.id.loginIn));
        viewsToFadeIn.add(findViewById(R.id.passwordIn));
        viewsToFadeIn.add(findViewById(R.id.loginText));
        viewsToFadeIn.add(findViewById(R.id.passwordText));
        viewsToFadeIn.add(findViewById(R.id.viewPager));

        for (View v : viewsToFadeIn)
        {
            v.setAlpha(0);
        }

        for (View v : viewsToFadeIn)
        {
            v.animate().alpha(1.0f).setDuration(1500).start();
        }

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginUser();
            }
        });

            //Przekierowanie na strone do rejestracji
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, WebViewActivity.class);
                startActivity(intent);
                intent.putExtra("loginText",loginText.getText());
            }
        });
        }

    private void LoginUser() {
        EditText loginIn = findViewById(R.id.loginIn);
        EditText passwordIn = findViewById(R.id.passwordIn);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://51.68.136.95:8080")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        final Json jsonPlaceholderAPI = retrofit.create(Json.class);

        Call<User> call = jsonPlaceholderAPI.login(new AuthData(loginIn.getText().toString(), passwordIn.getText().toString()));

        call.enqueue(new Callback<User>() {


            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                SharedPreferences sharedPreferences = getSharedPreferences("shared preferences", MODE_PRIVATE);

                if (response.isSuccessful()) {
                    System.out.println(response.body().getId() + " #ZALOGOWANO# ");
                    User user = response.body();
                    Integer userId=user.getId();
                    String userLogin=user.getLogin();
                    Integer userPoints=user.getPoints();
                    System.out.println(user.getLogin());

                    Intent intent = new Intent(LoginActivity.this, Profile.class);
                    intent.putExtra(EXTRA_ID,userId);
                    intent.putExtra(EXTRA_POINTS,userPoints);
                    intent.putExtra(EXTRA_LOGIN,userLogin);
                    startActivity(intent);
                }
            }
            @Override
            public void onFailure(Call<User> call, Throwable t) {
                System.out.println("++++++++++++" + t.getMessage());
                Context context = getApplicationContext();
                int duration = Toast.LENGTH_LONG;
                Toast toast = Toast.makeText(context,"Niepoprawne Dane!!", duration);
                System.out.println("NIEPOPRAWNE DANE");
                toast.show();
            }
        });
    }
}
