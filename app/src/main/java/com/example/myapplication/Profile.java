package com.example.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.kofigyan.stateprogressbar.StateProgressBar;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Profile extends AppCompatActivity {
    public static final String EXTRA_ID ="com.example.myapplication.EXTRA_ID";
    private Button settings;
    private Button toGoals;
    private Button toGoalsTwo;
    private ArrayList<String> goalNames = new ArrayList<>();
    private ArrayList<StateProgressBar.StateNumber>  listOfProgressBar = new ArrayList<>();
    private Integer id;
    private Integer points;
    final Handler handler = new Handler();

    private RecyclerView.LayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_activity);
        Intent intent=getIntent();
        id= intent.getIntExtra(LoginActivity.EXTRA_ID, 0);
        points= intent.getIntExtra(LoginActivity.EXTRA_POINTS, 0);
        String login=intent.getStringExtra(LoginActivity.EXTRA_LOGIN);
        TextView textViewNamee=findViewById(R.id.textViewName);
        textViewNamee.setText(login);
        TextView textViewPoints=findViewById(R.id.your_points);
        textViewPoints.setText(""+points);
        toGoals = findViewById(R.id.to_your_goals);
        toGoalsTwo = findViewById(R.id.to_u_goal_two);
        toGoalsTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Profile.this,YourGoalFragmentTwo.class);
                startActivity(intent);
            }
        });
        toGoals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Profile.this,YourGoalFragment.class);
                startActivity(intent);
            }
        });

        settings = findViewById(R.id.settings_icon);
        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchSettings(id);
            }
        });
    }

    private void launchSettings(int id) {
        Intent intent = new Intent(Profile.this, SettingsActivity.class);
        intent.putExtra(EXTRA_ID,id);
        startActivity(intent);
    }
}
