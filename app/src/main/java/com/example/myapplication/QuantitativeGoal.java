package com.example.myapplication;

import java.util.Date;

public class QuantitativeGoal {
    private Integer id;
    private Integer id_user;
    private Date date;
    private boolean is_shared;
    private String name;
    private String description;
    private String goal;
    private Integer days;
    private Integer points;
    private String progress;

    public QuantitativeGoal(Integer id, Integer id_user, Date date, boolean is_shared, String name, String description, String goal, Integer days, Integer points, String progress, Integer target, Integer step) {
        this.id = id;
        this.id_user = id_user;
        this.date = date;
        this.is_shared = is_shared;
        this.name = name;
        this.description = description;
        this.goal = goal;
        this.days = days;
        this.points = points;
        this.progress = progress;
        this.target = target;
        this.step = step;
    }
    public QuantitativeGoal(){
        System.out.println("Bezargumentowy");
    }

    private Integer target;
    private Integer step;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId_user() {
        return id_user;
    }

    public void setId_user(Integer id_user) {
        this.id_user = id_user;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public boolean isIs_shared() {
        return is_shared;
    }

    public void setIs_shared(boolean is_shared) {
        this.is_shared = is_shared;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getGoal() {
        return goal;
    }

    public void setGoal(String goal) {
        this.goal = goal;
    }

    public Integer getDays() {
        return days;
    }

    public void setDays(Integer days) {
        this.days = days;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public String getProgress() {
        return progress;
    }

    public void setProgress(String progress) {
        this.progress = progress;
    }

    public Integer getTarget() {
        return target;
    }

    public void setTarget(Integer target) {
        this.target = target;
    }

    public Integer getStep() {
        return step;
    }

    public void setStep(Integer step) {
        this.step = step;
    }
}
