package com.example.myapplication;

public class QuantitativeGoalData {
    private Integer id_user;
    private boolean is_shared;
    private String name;
    private String description;
    private String goal;
    private Integer days;
    private Integer step;

    public QuantitativeGoalData(Integer id_user, boolean is_shared, String name, String description, String goal, Integer days, Integer step) {
        this.id_user = id_user;
        this.is_shared = is_shared;
        this.name = name;
        this.description = description;
        this.goal = goal;
        this.days = days;
        this.step = step;
    }
    public QuantitativeGoalData(){
        System.out.println("Bezargumentowy");
    }


    public Integer getId_user() {
        return id_user;
    }

    public void setId_user(Integer id_user) {
        this.id_user = id_user;
    }

    public boolean isIs_shared() {
        return is_shared;
    }

    public void setIs_shared(boolean is_shared) {
        this.is_shared = is_shared;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getGoal() {
        return goal;
    }

    public void setGoal(String goal) {
        this.goal = goal;
    }

    public Integer getDays() {
        return days;
    }

    public void setDays(Integer days) {
        this.days = days;
    }

    public Integer getStep() {
        return step;
    }

    public void setStep(Integer step) {
        this.step = step;
    }
}
