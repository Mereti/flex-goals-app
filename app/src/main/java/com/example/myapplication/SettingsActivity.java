package com.example.myapplication;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SettingsActivity extends AppCompatActivity {


    RadioGroup RadioGroupSex;
    RadioButton RadioButton;
    EditText editTextName;
    public UserData data;
    private  Button applyChanges;
    //EditText editTextDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        Intent intent=getIntent();
        getSupportActionBar().hide();
        int id= intent.getIntExtra(LoginActivity.EXTRA_ID, 0);

        RadioGroupSex=findViewById(R.id.RadioGroupSex);
        editTextName=findViewById(R.id.editTextName);
        //editTextDate=findViewById(R.id.editTextDate);
         applyChanges=findViewById(R.id.applyChanges);

        applyChanges.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int radioId=RadioGroupSex.getCheckedRadioButtonId();
                RadioButton=findViewById(radioId);
                String gender= (String) RadioButton.getText();
                String name=editTextName.getText().toString();

                if(gender.equals("Mężczyzna")){
                    data=new UserData(id, ""+name,true);
                }else{
                    data=new UserData(id, ""+name,false);
                }

                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl("http://artsit.pl:8080")
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();

                final Json jsonPlaceholderAPI = retrofit.create(Json.class);

                Call<UserData> call = jsonPlaceholderAPI.updateUserName(data);
                Call<UserData> call2=jsonPlaceholderAPI.updateUserSex(data);

                call.enqueue(new Callback<UserData>() {

                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(Call<UserData> call, Response<UserData> response) {
                        SharedPreferences sharedPreferences = getSharedPreferences("shared preferences", MODE_PRIVATE);

                        if (!response.isSuccessful()) {
                            System.out.println("##### Code "+ response.code());
                            return;
                        }
                        System.out.println(data.getName()+"### IMIE ZMIENIONE ###");
                        System.out.println(data.isSex()+"### PLEC ZMIENIONA ###");


                    }

                    @Override
                    public void onFailure(Call<UserData> call, Throwable t) {

                    }

                });
                call2.enqueue(new Callback<UserData>() {
                    @Override
                    public void onResponse(Call<UserData> call, Response<UserData> response) {
                        SharedPreferences sharedPreferences = getSharedPreferences("shared preferences", MODE_PRIVATE);
                        if (!response.isSuccessful()) {
                            System.out.println("##### Code "+ response.code());
                            return;
                        }
                        System.out.println(data.isSex()+"### PLEC ZMIENIONA ###");

                    }

                    @Override
                    public void onFailure(Call<UserData> call, Throwable t) {

                    }
                });
                Context context = getApplicationContext();
                CharSequence text = "IMIĘ "+data.getName()+" PŁEĆ "+ gender ;
                int duration = Toast.LENGTH_SHORT;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();


            }
        });

    }
    /*
    public void applyButton(View v){
        int radioId=RadioGroupSex.getCheckedRadioButtonId();
        RadioButton=findViewById(radioId);
        textViewSex.setText(""+radioId);
    }




     */
}