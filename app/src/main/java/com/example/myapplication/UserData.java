package com.example.myapplication;

import java.util.Date;

public class UserData {
    private Integer id;
    private String name;
    private Date date_of_birth;
    private boolean sex;

    public UserData(Integer id, String name, Date date_of_birth, boolean sex) {
        this.id = id;
        this.name = name;
        this.date_of_birth = date_of_birth;
        this.sex = sex;
    }
    public UserData(Integer id, String name, boolean sex) {
        this.id = id;
        this.name = name;
        this.sex = sex;
    }
    public UserData(){
        System.out.println("Bezargumentowy");
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDate_of_birth() {
        return date_of_birth;
    }

    public void setDate_of_birth(Date date_of_birth) {
        this.date_of_birth = date_of_birth;
    }

    public boolean isSex() {
        return sex;
    }

    public void setSex(boolean sex) {
        this.sex = sex;
    }
}
