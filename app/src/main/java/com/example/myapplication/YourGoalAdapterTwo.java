package com.example.myapplication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;

public class YourGoalAdapterTwo extends RecyclerView.Adapter<YourGoalAdapterTwo.ViewHolder> {

    private static final String TAG ="RecyclerViewAdapter";
    private List<QuantitativeGoalFlag> goalsNamesList = new ArrayList<>();
    private Context context;
    public YourGoalAdapterTwo(List<QuantitativeGoalFlag> goalsNamesList, Context context) {
        this.goalsNamesList = goalsNamesList;
        this.context = context;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.example_content, parent, false);
        YourGoalAdapterTwo.ViewHolder holder = new YourGoalAdapterTwo.ViewHolder(view);

        return holder;
    }
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        QuantitativeGoalFlag positionOne = goalsNamesList.get(position);
        holder.nameOfGoal.setText(positionOne.getName());
        System.out.println("++++" + positionOne);
        //---------------------------
        int ile_liter = 0;
        char litera = '0';
        char znak_z_napisu ;
        for (int i = 0 ; i < positionOne.getProgress().length() ; i++)
        {
            znak_z_napisu = positionOne.getProgress().charAt(i);
            if(znak_z_napisu == litera)
            {
                ile_liter++;
            }
        }

        //--------------------
        int percentage = (int) ((ile_liter/(float)positionOne.getDays() )*100);
        holder.yourStateProgressBarId.setProgress(percentage);
        holder.yourStateProgressBarId.setMax(100);
        // holder.yourStateProgressBarId.get
    }

    @Override
    public int getItemCount() {
        return goalsNamesList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView nameOfGoal;
        ProgressBar yourStateProgressBarId;
        ConstraintLayout parentLayout;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            nameOfGoal = itemView.findViewById(R.id.name_of_goal);
            yourStateProgressBarId = itemView.findViewById(R.id.your_state_progress_bar_id);
            parentLayout = itemView.findViewById(R.id.parent_layout);

        }
    }
}
