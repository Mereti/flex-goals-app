package com.example.myapplication;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Canvas;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.kofigyan.stateprogressbar.StateProgressBar;

import java.util.ArrayList;
import java.util.List;

import it.xabaras.android.recyclerview.swipedecorator.RecyclerViewSwipeDecorator;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class YourGoalFragment extends AppCompatActivity {

    private static final String TAG = "YourGoalFragment";
    private ArrayList<String> goalNames = new ArrayList<>();
    private ArrayList<StateProgressBar.StateNumber> listOfProgressBar = new ArrayList<>();
    private Button addGoal;
    private Integer id;
    Intent intent;
    final Handler handler = new Handler();


    YourGoalAdapter adapter;

    List<FinalGoalFlag> models;
    RecyclerView recyclerViewProfile;
    private RecyclerView.LayoutManager layoutManager;

    Context context;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.your_goal);


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://51.68.136.95:8080")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        final Json json = retrofit.create(Json.class);
        intent=getIntent();
        id= intent.getIntExtra(LoginActivity.EXTRA_ID, 0);
        Call<List<FinalGoalFlag>> call = json.getGoalsFlag(18);
        call.enqueue(new Callback<List<FinalGoalFlag>>() {
            @Override
            public void onResponse(Call<List<FinalGoalFlag>> call, Response<List<FinalGoalFlag>> response) {
                if (!response.isSuccessful()) {
                    System.out.println("!!Blad!!" );
                }

                System.out.println("--" + response.body());
                models = response.body();
                models.stream().forEach(a -> System.out.println(a.getId()));

                //ADAPTER--------------------------------
                adapter = new YourGoalAdapter(models, YourGoalFragment.this);
                recyclerViewProfile = findViewById(R.id.list_of_goals);
                layoutManager = new LinearLayoutManager(YourGoalFragment.this);
                recyclerViewProfile.setLayoutManager(layoutManager);
                if (recyclerViewProfile == null) {
                    System.out.println("PUSTA LISTAAAAA");
                }
                System.out.println("**DZIALA DZIALA**");

                        recyclerViewProfile.setAdapter(adapter);


                ItemTouchHelper.SimpleCallback callback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT | ItemTouchHelper.LEFT) {
                    @Override
                    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                        return false;
                    }
                    @Override
                    public void onChildDraw (Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder,float dX, float dY,int actionState, boolean isCurrentlyActive){

                        new RecyclerViewSwipeDecorator.Builder(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
                                .addSwipeRightBackgroundColor(ContextCompat.getColor(YourGoalFragment.this, R.color.mediumGreen))
                                .addSwipeRightActionIcon(R.drawable.white_add)
                                .addSwipeLeftBackgroundColor(ContextCompat.getColor(YourGoalFragment.this, R.color.to_delete))
                                .addSwipeLeftActionIcon(R.drawable.white_delete)
                                .create()
                                .decorate();
                        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
                    }
                    @Override
                    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                        int position = viewHolder.getAdapterPosition();
                        switch (direction) {
                            case ItemTouchHelper.LEFT:
                                //ALERTTTTT---------------------------------
                                AlertDialog.Builder builder1 = new AlertDialog.Builder(YourGoalFragment.this);
                                builder1.setMessage("Czy chcesz usunąć swój cel?");
                                builder1.setCancelable(true);

                                builder1.setPositiveButton(
                                        "Yes",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.cancel();
                                                Call<ResponseBody> call1 = json.deleteGoal(models.get(position).getId());
                                                System.out.println("OOOOOOOOOOOOOOOOOOOOOOOO" + models.get(position).getId());
                                                call1.enqueue(new Callback<ResponseBody>() {
                                                    @Override
                                                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                                        if (!response.isSuccessful()) {
                                                            System.out.println("BRAK POLACZENIA  - " + response.body());
                                                        }
                                                    }
                                                    @Override
                                                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                                                        System.out.println("NIE USUNIETO CELUUUUUU%%%%%%%%%%");
                                                    }
                                                });
                                                models.remove(position);
                                                adapter.notifyItemRemoved(position);
                                            }
                                        });

                                builder1.setNegativeButton(
                                        "No",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.cancel();
                                            }
                                        });

                                AlertDialog alert11 = builder1.create();
                                alert11.show();

                                break;
                            case ItemTouchHelper.RIGHT:
                                    //TODO:tutaj zaliczanie celów

                                break;
                        }
                    }
                };
                ItemTouchHelper itemTouchHelper = new ItemTouchHelper(callback);
                itemTouchHelper.attachToRecyclerView(recyclerViewProfile);
            }

            @Override
            public void onFailure(Call<List<FinalGoalFlag>> call, Throwable t) {
                System.out.println("______________-------------------------ONFAILURE");
                try {
                    throw t;
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                }
            }
        });

        //Add your goal----------------------------
        addGoal = findViewById(R.id.addBtn);
        addGoal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(YourGoalFragment.this, AddYourGoal.class);
                startActivity(intent);
            }
        });
    }


}
