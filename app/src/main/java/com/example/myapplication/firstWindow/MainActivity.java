package com.example.myapplication.firstWindow;

import android.animation.ArgbEvaluator;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.example.myapplication.Json;
import com.example.myapplication.LoginActivity;
import com.example.myapplication.R;
import com.example.myapplication.UserInfoDate;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;



public class MainActivity extends AppCompatActivity {

    String[] descriptionData = {"ŚWIETNY \n  POCZĄTEK", "keep it \n pushin", "Jeszcze \n trochę", "YAAAY"};

    private Button toMyGoals;
    ViewPager viewPager;
    ImageAdapter adapter;
    List<Model> models;
    Integer[] colors = null;
    ArgbEvaluator argbEvaluator = new ArgbEvaluator();
    RelativeLayout relativeLayout;
    private Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();

        View view = getLayoutInflater().inflate(R.layout.full_screen_app_pop_up,null);
        relativeLayout = view.findViewById(R.id.layout_full);
        dialog = new Dialog(this,android.R.style.Theme_DeviceDefault_Light_NoActionBar_Fullscreen);
        dialog.setContentView(view);
        dialog.show();
        Toast.makeText(MainActivity.this,"Naciśnij żeby zamknąć", Toast.LENGTH_LONG).show();
        relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        toMyGoals=findViewById(R.id.goThere);

        toMyGoals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });

        //View Pager-------------------------------------
        models = new ArrayList<>();
        models.add(new Model(R.drawable.mike,"Inspiracja","Z NAMI ODKRYJESZ SIEBIE! \n\nZnajdź z nami to co cię napedza. "));
        models.add(new Model(R.drawable.image_five,"Sport","Poszerzaj swoje granice"));
        models.add(new Model(R.drawable.lora,"Organizer","Twój osobisty menadzer, bedzie pomocnikiem w skutecznym planowaniu dnia"));
        models.add(new Model(R.drawable.image_four,"Motywacja","Zmień swojeń życie na lepsze z aplikacją Flexgo. \n \n POMOŻEMY CI SPEŁNIĆ MARZENIA!!"));
        models.add(new Model(R.drawable.chester_wade,"Cele"," Codzienne przypomnienie o twoich celach pomoże ci nie zapomniec do kąd zmierzasz "));

        //adapter = new Adapter(models,MainActivity.this);
        adapter = new ImageAdapter(models, MainActivity.this);
        viewPager = findViewById(R.id.viewPager);
        viewPager.setAdapter(adapter);
        viewPager.setPadding(130,0,130,0);

        Integer[] colors_temp = {
                getResources().getColor(R.color.color_one),
                getResources().getColor(R.color.color_two),
                getResources().getColor(R.color.color_three),
                getResources().getColor(R.color.color_four),
                getResources().getColor(R.color.color_five)
        };
        colors = colors_temp;
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener(){
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                if(position < (adapter.getCount() - 1) && position < (colors.length - 1 )){
                    viewPager.setBackgroundColor((Integer) argbEvaluator.evaluate(
                            positionOffset,
                            colors[position],
                            colors[position + 1]
                            )
                    );
                }else {
                    viewPager.setBackgroundColor(colors[colors.length - 1]);
                }
            }
            @Override
            public void onPageSelected(int position) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://51.68.136.95:8080")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        final Json jsonPlaceholderAPI = retrofit.create(Json.class);
        Call<UserInfoDate> call = jsonPlaceholderAPI.getLogin("MARCINEK", "qwe");
        call.enqueue(new Callback<UserInfoDate>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(Call<UserInfoDate> call, Response<UserInfoDate> response) {
                SharedPreferences sharedPreferences = getSharedPreferences("shared preferences", MODE_PRIVATE);
                if(!response.isSuccessful()){
                  //  System.out.println(response.body().getId()+ "BLAD ");
                    return;
                }
                UserInfoDate userInfoDate = response.body();
            }
            @Override
            public void onFailure(Call<UserInfoDate> call, Throwable t) {
                String noCon = "++++--BRAK POLĄCZENIA--++++ "+t.getMessage()+" --BRAK POLĄCZENIA--";
                System.out.println(noCon);
                Context context = getApplicationContext();
                int duration = Toast.LENGTH_LONG;
                Toast toast = Toast.makeText(context,noCon,duration);
             //   toast.show();
            }
        });

    }
}
